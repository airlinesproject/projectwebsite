﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="user_signup_page_with_master_page.aspx.cs" Inherits="ELibrarymanagement.user_signup_page_with_master_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">

    <div class="container">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                    <img width="150px" src="img_login/useme.png" />
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Sign Up Form</h3>
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label">Full Name</label>
                                    <asp:TextBox ID="TextBox10" CssClass="form-control" Placeholder="Full Name" runat="server"></asp:TextBox>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date Of Birth</label>
                                    <asp:TextBox ID="TextBox2" TextMode="Date" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Contact Number</label>
                                    <asp:TextBox ID="TextBox3" TextMode="Number" CssClass="form-control" Placeholder="enter phone number" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Email Id</label>
                                    <asp:TextBox ID="TextBox4" TextMode="Email" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label >State</label>
                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
                                        <%--listitem--%>
                                        <asp:ListItem Text="select" Value="select"></asp:ListItem>
                                        <asp:ListItem Text="Province 1" Value="Province 1"></asp:ListItem>
                                        <asp:ListItem Text="Province 2" Value="Province 2"></asp:ListItem>
                                        <asp:ListItem Text="Province 3" Value="Province 3"></asp:ListItem>
                                        <asp:ListItem Text="Province 4" Value="Province 4"></asp:ListItem>
                                        <asp:ListItem Text="Province 5" Value="Province 5"></asp:ListItem>
                                        <asp:ListItem Text="Province 6" Value="Province 6"></asp:ListItem>
                                        <asp:ListItem Text="Province 7" Value="Province 7"></asp:ListItem>

                                        <%--listitem--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label >City</label>
                                    <asp:TextBox ID="TextBox6" Placeholder="City" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gender</label>
                                    <asp:RadioButton ID="RadioButton1" GroupName="gender" runat="server" Text="Male" />
                                    <asp:RadioButton ID="RadioButton2" GroupName="gender" runat="server" Text="Female" />
                                    <%--<asp:TextBox ID="TextBox7" Placeholder="Pin Code" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                </div>
                            </div>
                        </div>
                        <%--row--%>


                        <%--row--%>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label >Full Address</label>
                                    <asp:TextBox ID="TextBox8" TextMode="MultiLine" placeholder="enter your full and precise address" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <%--row--%>

                        <%--row--%>

                        <div class="row">

                            <div class="col">
                                <center>    
                                <span class="badge badge-pill badge-info"> Login Credentials </span>
                                </center>
                            </div>

                        </div>


                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >User Name</label>
                                    <asp:TextBox ID="TextBox5" placeholder="choose a UserName" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label >Password</label>
                                    <asp:TextBox ID="TextBox9" TextMode="Password" placeholder="enter your Password" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <%--row--%>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <asp:Button ID="Button1" class="btn btn-success btn-block btn-lg" runat="server" Text="Sign Up" OnClick="Button1_Click" />
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>

        </div>
    </div>

</asp:Content>
