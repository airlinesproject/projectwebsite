﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="admin_login_with_master_page.aspx.cs" Inherits="ELibrarymanagement.admin_login_with_master_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <img width="150px" src="img_login/adminlogo.png" />
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Admin Login</h3>
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                 <div class="form-group">

                                    <asp:TextBox ID="TextBox1" Placeholder="Username" class="form-control" runat="server"></asp:TextBox>
                                </div>

                                <div class="form-group">

                                    <asp:TextBox ID="TextBox2" class="form-control" Placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                                </div>

                                <div class="form-group">
                                    <asp:Button class="btn btn-success btn-block btn-lg" ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
                                </div>

                                <div class="form-group">
                                    <asp:Button class="btn btn-primary btn-block btn-lg" ID="Button2" runat="server" Text="Sign Up" />
                                </div>
                            </div>
                        </div>
                    </div>

                <%--</div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>--%>

        </div>
    </div>
            </div>
        <br />
            
</asp:Content>
