﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="membermanagement.aspx.cs" Inherits="ELibrarymanagement.membermanagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
    $(document).ready(function() {
    $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();
    
    });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <%--code from sales pasting--%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body px-1">

                        <%--member row text--%>
                        <div class="row">
                            <div class="col">
                                <center>
                           <h4>Member Details</h4>
                        </center>
                            </div>
                        </div>

                        <%--image row--%>
                        <div class="row">
                            <div class="col">
                                <center>
                           <img width="70px" src="img_login/useme.png" />
                        </center>
                            </div>
                        </div>

                        <%--hr row--%>
                        <div class="row">
                            <div class="col">
                                <hr>
                            </div>
                        </div>

                        <%--1st input row--%>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Member ID</label>
                                <div >
                                    <div class="input-group">
                                        <asp:TextBox CssClass="form-control" ID="TextBox1" runat="server" placeholder="ID"></asp:TextBox>
                                        <asp:LinkButton class="btn btn-primary" ID="LinkButton4" runat="server" OnClick="LinkButton4_Click"><i class="fas fa-check-circle"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Full Name</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox2" runat="server" placeholder="Full Name" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Account Status</label>
                                <div >
                                    <div class="input-group">
                                        <asp:TextBox CssClass="form-control " ID="TextBox7" runat="server" placeholder="Status" ReadOnly="True"></asp:TextBox>
                                        <asp:LinkButton class="btn btn-success px-0" ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"><i class="fas fa-check-circle"></i></asp:LinkButton>
                                        <asp:LinkButton class="btn btn-warning px-0" ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"><i class="far fa-pause-circle"></i></asp:LinkButton>
                                        <asp:LinkButton class="btn btn-danger px-0" ID="LinkButton3" runat="server" OnClick="LinkButton3_Click"><i class="fas fa-times-circle"></i></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--2nd input row--%> 
                        <div class="row">
                            <div class="col-md-3">
                                <label>DOB</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox8" runat="server" placeholder="DOB" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Contact No</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox3" runat="server" placeholder="Contact No" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label>Email ID</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox4" runat="server" placeholder="Email ID" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <%--3rd input row--%> 
                        <div class="row">
                            <div class="col-md-4">
                                <label>State</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox9" runat="server" placeholder="State" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>City</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox10" runat="server" placeholder="City" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Pin Code</label>
                                <div >
                                    <asp:TextBox CssClass="form-control" ID="TextBox11" runat="server" placeholder="Pin Code" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <%--4th input row--%>
                        <div class="row">
                            <div class="col-12">
                                <label>Full Postal Address</label>
                                <div>
                                    <asp:TextBox CssClass="form-control" ID="TextBox6" runat="server" placeholder="Full Postal Address" TextMode="MultiLine" Rows="2" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <br />

                        <%--button for delete user row--%>
                        <div class="row">
                            <div class="col-8 mx-auto">
                                <asp:Button ID="Button2" class="btn btn-lg btn-block btn-danger" runat="server" Text="Delete User Permanently" OnClick="Button2_Click" />
                            </div>
                        </div>

                    </div>
                </div>
                <a href="homepage.aspx"><< Back to Home</a><br>
                <br>
            </div>

            <div class="col-md-7 px-1">
                <div class="card ">
                    <div class="card-body text-left">
                        <div class="row">
                            <div class="col">
                                <center>
                           <h4>Member List</h4>
                        </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server"  ConnectionString="<%$ ConnectionStrings:switchonConnectionString %>" SelectCommand="SELECT * FROM [member_master_tbl]"></asp:SqlDataSource>--%>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:switchonConnectionString %>" SelectCommand="SELECT * FROM [member_master_tbl]"></asp:SqlDataSource>
                            <div style="overflow: auto" class="col">
                                           
                                <asp:GridView  class="table table-striped table-bordered" ID="GridView1" runat="server"  AutoGenerateColumns="False" DataKeyNames="member_id" DataSourceID="SqlDataSource1">
                                    <Columns>
                                        <asp:BoundField DataField="member_id" HeaderText="UId" ReadOnly="True" SortExpression="member_id" />
                                        <asp:BoundField DataField="fullname" HeaderText="NAME" SortExpression="fullname" />
                                        <asp:BoundField DataField="account_status" HeaderText="STATUS" SortExpression="account_status" />
                                        <asp:BoundField DataField="email" HeaderText="EMAIL" SortExpression="email" />
                                        <asp:BoundField DataField="contact" HeaderText="CONTACT" SortExpression="contact" />
                                        <asp:BoundField DataField="state" HeaderText="STATE" SortExpression="state" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--code from sales pasting--%>
</asp:Content>
