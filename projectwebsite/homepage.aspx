﻿<%-- content page of master page --%>

<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="homepage.aspx.cs" Inherits="ELibrarymanagement.homepage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <section>
        <img src="imgs/widescreen2.png" class="img-fluid" />
    </section>

    <%-- 3 features Open--%>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <center>
                        <h1>Our Features</h1>
                        <p><b>Our main 3 Features</b></p>
                    </center>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <center>
                        <img src="imgs/office.png" width="70" height="70" />
                        <h4>Digital Photo Studio</h4>
                        <p><b>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</b></p>
                    </center>
                </div>

                <div class="col-md-4">
                    <center>
                        <img src="imgs/flag.png" width="70" height="70" />
                        <h4>Of Nepal</h4>
                        <p><b>qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt.</b></p>
                    </center>
                </div>


                <div class="col-md-4">
                    <center>
                        <img src="imgs/printer.png" width="70" height="70" />
                        <h4>With Advance Printers</h4>
                        <p><b>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</b></p>
                    </center>
                </div>


            </div>



        </div>
    </section>
    <%-- 3 features End--%>

    <%-- Banner In Between Open --%>
    
    <img src="imgs/banner3.jpg" class="img-fluid" />

    <%-- Banner In Between Open --%>

    <%-- (copied upper section)3 features as admin page Open--%>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <center>
                        <h1>Pages for admin</h1>
                        
                    </center>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <center>
                        <img src="imgs/office.png" width="70" height="70" />
                        <h4>Add content</h4>
                        <p><b>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.</b></p>
                    </center>
                </div>

                <div class="col-md-4">
                    <center>
                        <img src="imgs/flag.png" width="70" height="70" />
                        <h4>Add Product</h4>
                        <p><b>qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt.</b></p>
                    </center>
                </div>


                <div class="col-md-4">
                    <center>
                        <img src="imgs/printer.png" width="70" height="70" />
                        <h4>Add Enquiry</h4>
                        <p><b>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</b></p>
                    </center>
                </div>


            </div>



        </div>
    </section>

    <%--  (copied upper section)3 features as admin page End --%>
    <section>
        <img src="imgs/widescreen.jpg" class="img-fluid" />
    </section>

</asp:Content>
