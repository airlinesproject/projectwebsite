﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELibrarymanagement
{
    public partial class user_signup_page_with_master_page : System.Web.UI.Page
    {
        SqlConnection conn = new SqlConnection();


        protected void Page_Load(object sender, EventArgs e)
        {

        }



        //sign up button events
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (checkMemberExist())
            {
                Response.Write("<script>alert('the member exists already. Pick another id'); </script>");
            }
            else
            {
                newsignup();

            }
        }

        void newsignup()
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                //getting string from textbox dropdown list radio button.
                string fullname = TextBox10.Text;
                string db = TextBox2.Text;
                string contact = TextBox3.Text;
                string email = TextBox4.Text;
                string state = DropDownList1.SelectedItem.ToString();

                string city = TextBox6.Text;

                //gender selection code
                string gender = null;
                if (RadioButton1.Checked)
                {
                    gender = "male";
                }
                else if (RadioButton2.Checked)
                {
                    gender = "female";
                }
                string fulladdress = TextBox8.Text;
                string member_id = TextBox5.Text;
                string password = TextBox9.Text;
                string account_status = "pending";


                //insert query
                string strsql = "INSERT INTO member_master_tbl(fullname, dob, contact, email, state, city, gender, fulladdress, member_id, password, account_status) VALUES('" + fullname + "','" + db + "','" + contact + "','" + email + "','" + state + "','" + city + "','" + gender + "','" + fulladdress + "','" + member_id + "','" + password + "','" + account_status + "')";


                SqlCommand cmd = new SqlCommand(strsql, conn);
                cmd.ExecuteNonQuery();
                conn.Close();

                //success message
                Response.Write("<script> alert('sign up successful. Go to login page and login'); </script>");


            }
            catch (Exception ex)
            {
                Response.Write("<script> alert('" + ex.Message + "'); </script>");

            }
        }

        //user define method

        bool checkMemberExist()
        {
            try
            {
                //connectio code
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
                    //checking connection    
                    if (conn.State == ConnectionState.Closed)
                        {
                            conn.Open();
                        }

                //end of connection code
                //select query
                string strsql = "Select * from member_master_tbl Where member_id='" + TextBox5.Text + "'";
               

                SqlCommand cmd = new SqlCommand(strsql, conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "')</script>");
                return false;
            }

        }



    }
}