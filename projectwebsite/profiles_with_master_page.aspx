﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="profiles_with_master_page.aspx.cs" Inherits="ELibrarymanagement.profiles_with_master_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">

    <%--userlogincodehere--%>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <img width="100px" src="img_login/useme.png" />
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Profile View</h3>
                                    <p>Account Status-<asp:Label ID="Label1" class="badge badge-pill badge-success" runat="server" Text="active"></asp:Label></p>
                                    
                            </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Full Name</label>
                                    <asp:TextBox ID="TextBox1" CssClass="form-control" Placeholder="Full Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Date Of Birth</label>
                                    <asp:TextBox ID="TextBox2" TextMode="Date" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Contact Number</label>
                                    <asp:TextBox ID="TextBox3" TextMode="Number" CssClass="form-control" Placeholder="enter phone number" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Email Id</label>
                                    <asp:TextBox ID="TextBox4" TextMode="Email" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">State</label>
                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
                                        <%--listitem--%>
                                        <asp:ListItem Text="select" Value="select"></asp:ListItem>
                                        <asp:ListItem Text="Province 1" Value="Province 1"></asp:ListItem>
                                        <asp:ListItem Text="Province 2" Value="Province 2"></asp:ListItem>
                                        <asp:ListItem Text="Province 3" Value="Province 3"></asp:ListItem>
                                        <asp:ListItem Text="Province 4" Value="Province 4"></asp:ListItem>
                                        <asp:ListItem Text="Province 5" Value="Province 5"></asp:ListItem>
                                        <asp:ListItem Text="Province 6" Value="Province 6"></asp:ListItem>
                                        <asp:ListItem Text="Province 7" Value="Province 7"></asp:ListItem>

                                        <%--listitem--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">City</label>
                                    <asp:TextBox ID="TextBox6" Placeholder="City" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Pin Code</label>
                                    <asp:TextBox ID="TextBox7" Placeholder="Pin Code" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--row--%>


                        <%--row--%>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Full Address</label>
                                    <asp:TextBox ID="TextBox8" TextMode="MultiLine" placeholder="enter your full and precise address" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <%--row--%>

                        <%--row--%>

                        <div class="row">

                            <div class="col">
                                <center>    
                                <span class="badge badge-pill badge-info"> Login Credentials </span>
                                </center>
                            </div>

                        </div>


                        <%--row--%>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">User Name</label>
                                    <asp:TextBox ID="TextBox5" placeholder="choose a UserName" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Old Password</label>
                                    <asp:TextBox ID="TextBox9" TextMode="Password" placeholder="old Password" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">New Password</label>
                                    <asp:TextBox ID="TextBox10" TextMode="Password" placeholder="new password" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                        <%--row--%>

                        <div class="row">
                            <div class="col">
                                <form>



                                    <div class="form-group">
                                        <asp:Button class="btn btn-success btn-block btn-lg" ID="Button2" runat="server" Text="Update" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>

            <div class="col-md-7 mx-auto">

                <%--copied code--%>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <img width="100px" src="img_login/photoasbooks.jpg" />
                            </center>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col">
                                <center>
                                <h3>Data View</h3>
                                    <p><asp:Label ID="Label2" class="badge badge-pill badge-success" runat="server" Text="Tabulated Data"></asp:Label></p>
                                    
                            </center>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col">

                                <asp:GridView ID="GridView1" class="table table-striped table-bordered" runat="server">

                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <%--copied code--%>
        </div>
    </div>



    <br />

    <%--userlogincodehere--%>
</asp:Content>
