﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELibrarymanagement
{
    public partial class Product_Management : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }
       

       
       
        void getProductById()
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
                //checking connection    
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                string strselect = "Select * from product where productid='" + TextBox1.Text + "'";

                SqlCommand cmd = new SqlCommand(strselect, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        TextBox2.Text = dr.GetValue(1).ToString();
                    }

                }
                else
                {
                    Response.Write("<script> alert('No such id found'); </script>");
                }
            }
            catch (Exception ex)
            {

                Response.Write("<script> alert('"+ex.Message+"');</script>");
            }
            
        }
        void clearForm()
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
        }
        //insert product
        void insertProduct()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
            //checking connection    
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string productid =TextBox1.Text;
            string productname = TextBox2.Text;

            string strselect = "INSERT INTO product(productid,productname) VALUES('" + productid + "','" + productname + "')"; 

            SqlCommand cmd = new SqlCommand(strselect, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            GridView1.DataBind();
            clearForm();
            Response.Write("<script>alert('product added successfully');</script>");
            
        }
        //update product
        void updateProduct()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
            //checking connection    
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string productid = TextBox1.Text;
            string productname = TextBox2.Text;

            string strselect = "UPDATE product SET productname='" + productname + "' WHERE productid='"+productid+"' ";

            SqlCommand cmd = new SqlCommand(strselect, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            GridView1.DataBind();
            Response.Write("<script>alert('Successfully Updated');</script>");
            clearForm();
        }
        //delete
        void deleteProduct()
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
                //checking connection    
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                string strselect = "Delete from product WHERE productid ='" + TextBox1.Text + "' ";

                SqlCommand cmd = new SqlCommand(strselect, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                GridView1.DataBind();
                Response.Write("<script>alert('product deleted permanently');</script>");
                clearForm();
            }
            catch (Exception ex)
            {

                Response.Write("<script> alert('"+ex.Message+"');");
            }
            
        }

        // go button
        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {

                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = default;

            }
            else
            {
                TextBox1.BorderColor = default;
                getProductById();

            }

        }

        //add
        protected void Button1_Click1(object sender, EventArgs e)
        {
            if (TextBox1.Text == "" && TextBox2.Text=="")
            {

                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = Color.Red;

            }
            else if (TextBox1.Text == "")
            {
                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = default;
            }

            else if (TextBox2.Text == "")
            {
                TextBox1.BorderColor = Color.Red;
            }
            else
            {
                TextBox1.BorderColor = default;
                insertProduct();
                clearForm();

            }
        }
        //update
        protected void Button3_Click1(object sender, EventArgs e)
        {
            if (TextBox1.Text == "" && TextBox2.Text == "")
            {

                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = Color.Red;

            }
            else if (TextBox1.Text == "")
            {

                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = default;

            }
            else if (TextBox2.Text == "")
            {
                TextBox1.BorderColor = Color.Red;
            }
            else
            {
                TextBox1.BorderColor = default;
                updateProduct();
                clearForm();

            }

        }

        //delete
        protected void Button4_Click1(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {

                TextBox1.BorderColor = Color.Red;

            }
            else if (TextBox2.Text == "")
            {
                TextBox1.BorderColor = Color.Red;
                TextBox2.BorderColor = default;
            }
            else
            {
                TextBox1.BorderColor = default;
                deleteProduct();
                clearForm();

            }
        }
    }
}