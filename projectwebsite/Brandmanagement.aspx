﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Brandmanagement.aspx.cs" Inherits="ELibrarymanagement.Brandmanagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Brand Details</h3>
                                    
                                    
                            </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <center>
                                <img width="100px" src="img_login/useme.png" />
                            </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Brand Id</label>
                                    <div class="input-group">

                                        <asp:TextBox ID="TextBox1" CssClass="form-control" Placeholder="ID" runat="server"></asp:TextBox>
                                        <asp:Button class="btn btn-success " ID="Button2" runat="server" Text="Go" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Brand Name</label>
                                    <asp:TextBox ID="TextBox2" CssClass="form-control" placeholder="brand" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <%--row Add Update DElete  Button--%>
                        <div class="row">
                            <div class="col-4">
                                <asp:Button ID="Button1" class="btn btn-lg btn-block btn-success" runat="server" Text="Add" />
                            </div>

                            <div class="col-4">
                                <asp:Button ID="Button3" class="btn btn-lg btn-block btn-warning" runat="server" Text="Update" />
                            </div>

                            <div class="col-4">
                                <asp:Button ID="Button4" class="btn btn-lg btn-block btn-danger" runat="server" Text="Delete" />
                            </div>
                        </div>
                        <%--row--%>


                        <div class="row">
                            <div class="col">
                                <form>



                                    <div class="form-group">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>

            <div class="col-md-7 mx-auto">

                <%--copied code--%>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Brand List</h3>
                                    
                                    
                            </center>
                            </div>
                        </div>


                        <%--line break--%>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col">
                                <asp:GridView ID="GridView1" class="table table-striped table-bordered" runat="server">
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <%--copied code--%>
        </div>
    </div>
</asp:Content>
