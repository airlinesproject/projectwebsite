﻿using Microsoft.AspNet.FriendlyUrls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELibrarymanagement
{
    public partial class membermanagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
            
        }

        // go button
        protected void LinkButton4_Click(object sender, EventArgs e)
        {
           
           
                getmemberbyid();
           
            
        }


        // active status button
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            updatememberstatus("active");
        }

        // pending status button
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            updatememberstatus("pending");
        }

        // deactive status button
        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            updatememberstatus("deactive");
        }

        //delete user permanently button
        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text == "")
            {

                TextBox1.BorderColor = Color.Red;
               
            }
            else
            {
                TextBox1.BorderColor = default;
                deleteUser();

            }
        }


        //get memberby id function
        void getmemberbyid()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
            //checking connection    
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string strselect = "Select * from member_master_tbl where member_id ='" + TextBox1.Text + "'";

            SqlCommand cmd = new SqlCommand(strselect, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    TextBox2.Text = dr.GetValue(0).ToString();
                    TextBox3.Text = dr.GetValue(2).ToString();
                    TextBox4.Text = dr.GetValue(3).ToString();
                    TextBox6.Text = dr.GetValue(7).ToString();
                    TextBox7.Text = dr.GetValue(10).ToString();
                    TextBox8.Text = dr.GetValue(1).ToString();
                    TextBox9.Text = dr.GetValue(4).ToString();
                    TextBox10.Text = dr.GetValue(5).ToString();
                    TextBox11.Text = dr.GetValue(6).ToString();
                }

            }
        }

        void updatememberstatus(string status)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
            //checking connection    
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string strselect = "UPDATE member_master_tbl SET account_status ='" + status + "' WHERE member_id ='"+TextBox1.Text+"' ";
            SqlCommand cmd = new SqlCommand(strselect, conn);
            cmd.ExecuteNonQuery();
            TextBox7.Text = status; //so that the text box is filled after update
            conn.Close();
            GridView1.DataBind();
            // Response.Write("<script>alert('member status updated');</script>");
        }

        void deleteUser()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=PAWAN;Initial Catalog=switchon;Integrated Security=True";
            //checking connection    
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }

            string strselect = "Delete from member_master_tbl WHERE member_id ='" + TextBox1.Text + "' ";

            SqlCommand cmd = new SqlCommand(strselect, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
            GridView1.DataBind();
            Response.Write("<script>alert('member deleted permanently');</script>");
            clearForm();
        }

        //clearing textboxes and all
        void clearForm()
        {
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            TextBox6.Text = "";
            TextBox7.Text = "";
            TextBox8.Text = "";
            TextBox9.Text = "";
            TextBox10.Text = "";
            TextBox11.Text = "";
           
        }
    }
}