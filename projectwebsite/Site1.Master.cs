﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELibrarymanagement
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string sessionrole = Session["role"] as string;
                if (string.IsNullOrEmpty(sessionrole))
                {
                 
                    user.Visible = true; //user login link button
                    signup.Visible = true; //signup link button

                    logout.Visible = false; // logout link button
                    hello.Visible = false; // hello user 

                    linkbutton1.Visible = true; // admin login of footer 

                    linkbutton2.Visible = false; // productmanagement of footer 
                    linkbutton3.Visible = false; // brandmanagement of footer 
                    linkbutton4.Visible = false; // stock inventory of footer 
                    linkbutton5.Visible = false; // Sales of footer 
                    linkbutton6.Visible = false; // Member management login of footer 
                }
                else if (Session["role"].Equals("user"))
                {
                    user.Visible = false; //user login link button
                    signup.Visible = false; //signup link button

                    hello.Visible = true; // hello user 
                    hello.Text = "Hello," + Session["username"].ToString();
                    
                    logout.Visible = true; // logout link button

                    linkbutton1.Visible = true; // admin login of footer 

                    linkbutton2.Visible = false; // productmanagement of footer 
                    linkbutton3.Visible = false; // brandmanagement of footer 
                    linkbutton4.Visible = false; // stock inventory of footer 
                    linkbutton5.Visible = false; // Sales of footer 
                    linkbutton6.Visible = false; // Member management login of footer 
                }
                else if (Session["role"].Equals("admin"))
                {
                    user.Visible = false; //user login link button
                    signup.Visible = false; //signup link button

                    hello.Visible = true; // hello user 
                    hello.Text = "Hello Admin";

                    logout.Visible = true; // logout link button


                    linkbutton1.Visible = false; // admin login of footer 

                    linkbutton2.Visible = true; // productmanagement of footer 
                    linkbutton3.Visible = true; // brandmanagement of footer 
                    linkbutton4.Visible = true; // stock inventory of footer 
                    linkbutton5.Visible = true; // Sales of footer 
                    linkbutton6.Visible = true; // Member management login of footer 
                }

            }
            catch (Exception ex)
            {
                Response.Write("'"+ex.Message+"'");
            }

            
        }
        protected void location_Click(object sender, EventArgs e)
        {

        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            Response.Redirect("homepage.aspx");
        }

       //Admin Login
        protected void linkbutton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("admin_login_with_master_page.aspx");
        }

        //Product Management
        protected void linkbutton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("productmanagement.aspx");
        }

        //brandmanagement
        protected void linkbutton3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Brandmanagement.aspx");
        }
        //stock inventory
        protected void linkbutton4_Click(object sender, EventArgs e)
        {
            Response.Redirect("stock_inventory.aspx");
        }
        //sales
        protected void linkbutton5_Click(object sender, EventArgs e)
        {
            Response.Redirect("sales.aspx");
        }
        //membermanagement
        protected void linkbutton6_Click(object sender, EventArgs e)
        {
            Response.Redirect("membermanagement.aspx");
        }


        //upper part linking
        //user login || login_wf_with_master_page
        protected void user_Click(object sender, EventArgs e)
        {
            Response.Redirect("login_wf_with_master_page.aspx");
        }

        //sign Up page
        protected void signup_Click(object sender, EventArgs e)
        {
            Response.Redirect("user_signup_page_with_master_page.aspx");
        }

        protected void view_Click(object sender, EventArgs e)
        {
            Response.Redirect("view.aspx");
        }

        //logout button code
        protected void logout_Click(object sender, EventArgs e)
        {
            Session["username"] = "";
            Session["fullname"] = "";
            Session["role"] = "";
            Session["status"] = "";

            user.Visible = true; //user login link button
            signup.Visible = true; //signup link button

            logout.Visible = false; // logout link button
            hello.Visible = false; // hello user 

            linkbutton1.Visible = true; // admin login of footer 

            linkbutton2.Visible = false; // productmanagement of footer 
            linkbutton3.Visible = false; // brandmanagement of footer 
            linkbutton4.Visible = false; // stock inventory of footer 
            linkbutton5.Visible = false; // Sales of footer 
            linkbutton6.Visible = false; // Member management login of footer
            Response.Redirect("homepage.aspx");

        }
    }
}