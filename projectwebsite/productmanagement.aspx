﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="productmanagement.aspx.cs" Inherits="ELibrarymanagement.Product_Management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
         $(document).ready(function () {
             $(".table").prepend($("<thead></thead>").append($(this).find("tr:first"))).dataTable();

         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Product Details</h3>
                            </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <center>
                                <img width= "100px" src="img_login/useme.png" />
                            </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col-md-5 px-1">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Product Id</label>
                                    <div class="input-group">

                                        <asp:TextBox ID="TextBox1" CssClass="form-control" Placeholder="ID" runat="server"></asp:TextBox>
                                        <asp:Button Cssclass="btn btn-success" ID="Button2" runat="server" Text="Go" OnClick="Button2_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Product Name</label>
                                    <asp:TextBox ID="TextBox2" CssClass="form-control" Placeholder="product" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <%--row Add Update DElete  Button--%>
                        <div class="row">
                            <div class="col-4">
                                <asp:Button ID="Button1" Cssclass="btn btn-lg btn-block btn-success" runat="server" Text="ADD" OnClick="Button1_Click1" />   
                            </div>

                            <div class="col-4">
                                <asp:Button ID="Button3" Cssclass="btn btn-lg btn-block btn-warning" runat="server" Text="UPDATE" OnClick="Button3_Click1" />   
                            </div>

                            <div class="col-4">
                                <asp:Button ID="Button4" Cssclass="btn btn-lg btn-block btn-danger" runat="server" Text="DELETE" OnClick="Button4_Click1" />   
                            </div>
                        </div>
                        <%--row--%>


                        
                    </div>

                </div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>

            <div class="col-md-7 mx-auto">

                <%--copied code--%>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Product List</h3>
                            </center>
                            </div>
                        </div>


                        <%--line break--%>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:switchonConnectionString %>" SelectCommand="SELECT * FROM [product]"></asp:SqlDataSource>
                            <div class="col">
                                <asp:GridView ID="GridView1" Cssclass="table table-striped table-bordered" runat="server" AutoGenerateColumns="False" DataKeyNames="productid" DataSourceID="SqlDataSource1">
                                    <Columns>
                                        <asp:BoundField DataField="productid" HeaderText="PRODUCT ID" ReadOnly="True" SortExpression="productid" />
                                        <asp:BoundField DataField="productname" HeaderText="PRODUCT" SortExpression="productname" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <%--copied code--%>
        </div>
    </div>
</asp:Content>
