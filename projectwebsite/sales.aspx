﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="sales.aspx.cs" Inherits="ELibrarymanagement.sales" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="contentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Sales</h3>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <center>
                                <img width="100px" src="img_login/useme.png" />
                            </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Customer Id</label>
                                    <asp:TextBox ID="TextBox2" CssClass="form-control" placeholder="ID" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Product Id</label>
                                    <div class="input-group">

                                        <asp:TextBox ID="TextBox1" CssClass="form-control" Placeholder="Product id" runat="server"></asp:TextBox>
                                        <asp:Button class="btn btn-success " ID="Button2" runat="server" Text="Go" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--custumername and productname--%>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Customer Name</label>
                                    <asp:TextBox ID="TextBox3" CssClass="form-control" ReadOnly="true" placeholder="Customer Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Product Name</label>
                                    <asp:TextBox ID="TextBox4" CssClass="form-control" ReadOnly="true" placeholder="Product Name" runat="server"></asp:TextBox>
                                </div>
                            </div>


                        </div>

                        <%--warranty start and end time--%>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Sales Date</label>
                                    <asp:TextBox ID="TextBox5" CssClass="form-control" placeholder="date of sales" TextMode="Date" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="formGroupExampleInput2">Warranty Till</label>
                                    <asp:TextBox ID="TextBox6" CssClass="form-control" placeholder="Warranty Time" TextMode="Date" runat="server"></asp:TextBox>
                                </div>
                            </div>


                        </div>


                        <%--row Add Update DElete  Button--%>
                        <div class="row">
                            <div class="col-6">
                                <asp:Button ID="Button1" class="btn btn-lg btn-block btn-success" runat="server" Text="Sale" />
                            </div>

                            <div class="col-6">
                                <asp:Button ID="Button3" class="btn btn-lg btn-block btn-warning" runat="server" Text="Sales Return" />
                            </div>

                        </div>
                        <%--row--%>


                        <div class="row">
                            <div class="col">
                                <form>



                                    <div class="form-group">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <a href="homepage.aspx"><< back to homepage </a>
            </div>

            <div class="col-md-7 mx-auto">

                <%--copied code--%>

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <center>
                                <h3>Sales List</h3>
                                    
                                    
                            </center>
                            </div>
                        </div>


                        <%--line break--%>
                        <div class="row">
                            <div class="col">
                                <hr />
                            </div>
                        </div>

                        <%--row--%>
                        <div class="row">
                            <div class="col">
                                <asp:GridView ID="GridView1" class="table table-striped table-bordered" runat="server">
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <%--copied code--%>
        </div>
    </div>
</asp:Content>
